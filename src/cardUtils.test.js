import {
    CardNumberErrors,
    CVVErrors,
    getCardType,
    isCardLengthValid,
    getCardNumberErrorMessage,
    getCVVErrorMessage,
    isCVVLengthValid,
} from './cardUtils.js';

describe('getCardType', function() {
    test('Card type should be VISA if first character is a 4', () => {
        expect(getCardType('4123')).toBe('VISA');
    });

    test('Card type should be AMEX if card number starts with 34', () => {
        expect(getCardType('3423')).toBe('AMEX');
    });

    test('Card type should be AMEX if card number starts with 37', () => {
        expect(getCardType('3723456789123456')).toBe('AMEX');
    });

    test('Card type should be OTHER if card number starts with 3X where X is not 4 or 7', () => {
        expect(getCardType('3023456789123456')).toBe('OTHER');
    });

    test('Card type should be OTHER if card number starts with something other than 3 or 4', () => {
        expect(getCardType('53023456789123456')).toBe('OTHER');
    });
});

describe('isCardLengthValid', () => {
    test('VISA card length should be invalid if less than 16 characters', () => {
        expect(isCardLengthValid('4123')).toBe(false);
    });

    test('VISA card length should be invalid if more than 16 characters', () => {
        expect(isCardLengthValid('41234567891234567')).toBe(false);
    });

    test('VISA card length should be valid if 16 characters', () => {
        expect(isCardLengthValid('4123456789123456')).toBe(true);
    });

    test('AMEX card length should be invalid if less than 15 characters', () => {
        expect(isCardLengthValid('3423')).toBe(false);
    });

    test('AMEX card length should be invalid if more than 15 characters', () => {
        expect(isCardLengthValid('3723456789123456')).toBe(false);
    });

    test('AMEX card length should be valid if 15 characters', () => {
        expect(isCardLengthValid('372345678912345')).toBe(true);
    });
});

describe('isCVVLengthValid', function() {
    test('VISA CVV length should be invalid if less than 3 characters', () => {
        expect(isCVVLengthValid('12', '4123')).toBe(false);
    });

    test('VISA CVV length should be invalid if more than 3 characters', () => {
        expect(isCVVLengthValid('1234', '4123')).toBe(false);
    });

    test('VISA CVV length should be valid if exactly 3 characters', () => {
        expect(isCVVLengthValid('123', '4123')).toBe(true);
    });

    test('AMEX CVV length should be invalid if less than 4 characters', () => {
        expect(isCVVLengthValid('123', '3412')).toBe(false);
    });

    test('AMEX CVV length should be invalid if more than 4 characters', () => {
        expect(isCVVLengthValid('12345', '3412')).toBe(false);
    });

    test('AMEX CVV length should be valid if exactly 4 characters', () => {
        expect(isCVVLengthValid('1234', '3412')).toBe(true);
    });

    test('OTHER CVV length should be valid', () => {
        expect(isCVVLengthValid('1234', '5412')).toBe(true);
    });
});

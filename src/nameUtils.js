function getNameErrorMessage(personName) {
    if(personName.length === 0) {
        return 'Please enter the name on the credit card';
    }
    return '';
}

export {getNameErrorMessage};
import React, { Component } from 'react';

import './App.css';

import CreditCardForm from './CreditCardForm.js'

class App extends Component {

  render() {
    return (
      <div id="main-app" className="App">
        <h1 id="main-header">Parent App</h1>
        <CreditCardForm/>
      </div>
    );
  }
}

export default App;

import React, { Component } from 'react';

// Material UI
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';

// Utilities
import {getCardNumberErrorMessage, getCVVErrorMessage} from './cardUtils.js';
import {getExpMonthOptions, getExpYearOptions, getExpDateError} from './expirationDateUtils.js';
import {getNameErrorMessage} from './nameUtils.js';

// Styles
const styles = theme => ({
    cardForm: {
        textAlign: 'center',
        border: '1px solid black',
        width: '500px',
    },
    nameInput: {
        width: '400px',
    },
    cardNumberInput: {
        width: '400px',
    },
    cvvInput: {
        width: '100px',
    },
    expDateContainer: {
        marginTop: '10px',
        marginBottom: '10px',
    },
    expMonthSelect: {
        width: '75px',
        paddingRight: '10px',
    },
    expYearSelect: {
        width: '75px',
        paddingLeft: '10px',
    },
    errorText: {
        textAlign: 'center',
    },
    submitButton: {
        paddingTop: '10px',
        paddingBottom: '10px',
    }
});

class CreditCardForm extends Component {
    constructor(props) {
        super(props);

        const currentDate = new Date();
        const currentYear = currentDate.getFullYear();
        const currentMonth = currentDate.getMonth() + 1;

        this.state = {
            personName: '',
            nameError: '',

            cardNumber: '',
            cardNumberError: '',

            cvv: '',
            cvvError: '',

            currentYear,
            currentMonth,
            expYearOptions: getExpYearOptions(currentYear),
            expMonthOptions: getExpMonthOptions(),
            expMonth: currentMonth,
            expYear: currentYear,
            expDateError: '',
        };

        this.handleCardNumberChange = this.handleCardNumberChange.bind(this);
        this.handleCardNumberBlur = this.handleCardNumberBlur.bind(this);
        
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleNameBlur = this.handleNameBlur.bind(this);

        this.handleExpMonthChange = this.handleExpMonthChange.bind(this);
        this.handleExpYearChange = this.handleExpYearChange.bind(this);

        this.handleCVVChange = this.handleCVVChange.bind(this);
        this.handleCVVBlur = this.handleCVVBlur.bind(this);
    }

    // Name

    handleNameChange(event) {
        let personName = event.target.value;

        this.setState({
            personName
        });

        // Clear existing errors if now valid
        if(!getNameErrorMessage(personName)) {
            this.setState({
                nameError: ''
            });
        }
    }

    handleNameBlur(event) {
        this.setState({
            nameError: getNameErrorMessage(this.state.personName)
        });
    }

    // Card number

    handleCardNumberChange(event) {
        let cardNumber = event.target.value;
        this.setState({
            cardNumber,
        });

        // Clear existing errors if now valid
        if(!getCardNumberErrorMessage(cardNumber)) {
            this.setState({
                cardNumberError: '',
            });
        }
        if(!getCVVErrorMessage(this.state.cvv, cardNumber)) {
            this.setState({
                cvvError: '',
            });
        }
    }

    handleCardNumberBlur(event) {
        let cardNumber = event.target.value;
        this.setState({
            cardNumberError: getCardNumberErrorMessage(cardNumber),
        });
    }

    // CVV2

    handleCVVChange(event) {
        let cvv = event.target.value;
        this.setState({
            cvv
        });

        // Clear existing errors if now valid
        if(!getCVVErrorMessage(cvv, this.state.cardNumber)) {
            this.setState({
                cvvError: ''
            });
        }
    }

    handleCVVBlur(event) {
        let cvv = event.target.value;
        this.setState({
            cvvError: getCVVErrorMessage(cvv, this.state.cardNumber)
        });
    }

    // Expiration date

    handleExpMonthChange(event) {
        let selectedMonth = event.target.value;
        this.setState({
            expMonth: event.target.value,
            expDateError: getExpDateError(
                this.state.currentYear,
                this.state.currentMonth,
                selectedMonth,
                this.state.expYear
            ),
        });
    }

    handleExpYearChange(event) {
        let selectedYear = event.target.value;
        this.setState({
            expYear: event.target.value,
            expDateError: getExpDateError(
                this.state.currentYear, 
                this.state.currentMonth, 
                this.state.expMonth, 
                selectedYear
            ),
        });
    }

    handleSubmit() {
        // TODO: focus on the first invalid input field
    }

    render() {
        const { classes } = this.props;
        return (
            <div className={classes.cardForm}>
                <form onSubmit={this.handleSubmit}>
                    <h2>Enter your credit card information</h2>
                    <div>
                        <TextField
                            id="name-input"
                            className={classes.nameInput}
                            error={!!this.state.nameError}
                            label="Name"
                            margin="normal"
                            variant="outlined"
                            onChange={this.handleNameChange}
                            onBlur={this.handleNameBlur}
                            helperText={this.state.nameError || ''}
                            FormHelperTextProps={
                                {
                                    'aria-live': 'assertive'
                                }
                            }
                            inputProps={
                                {
                                    'maxLength': 50,
                                    'aria-describedby': 'name-input-helper-text'
                                }
                            }
                        />
                    </div>

                    <div>
                        <TextField
                            id="credit-card-input"
                            className={classes.cardNumberInput}
                            error={!!this.state.cardNumberError}
                            label="Card number"
                            margin="normal"
                            variant="outlined"
                            onChange={this.handleCardNumberChange}
                            onBlur={this.handleCardNumberBlur}
                            value={this.state.cardNumber}
                            helperText={this.state.cardNumberError || ''}
                            FormHelperTextProps={
                                {
                                    'aria-live': 'assertive'
                                }
                            }
                            inputProps={
                                {
                                    'maxLength': 19,
                                    'aria-describedby': 'credit-card-input-helper-text'
                                }
                            }
                        />
                    </div>

                    <div>
                        <TextField
                            id="cvv-input"
                            className={classes.cvvInput}
                            label="CVV2"
                            margin="normal"
                            variant="outlined"
                            onChange={this.handleCVVChange}
                            onBlur={this.handleCVVBlur}
                            error={!!this.state.cvvError}
                            helperText={this.state.cvvError || ''}
                            FormHelperTextProps={
                                {
                                    'aria-live': 'assertive'
                                }
                            }
                            inputProps={
                                {
                                    maxLength: 4,
                                    'aria-describedby': 'cvv-input-helper-text'
                                }
                            }
                        />
                    </div>
                    <div className={classes.expDateContainer}>
                        <FormControl className={classes.expMonthSelect}>
                            <InputLabel htmlFor="exp-month">Exp month</InputLabel>
                            <Select
                                value={this.state.expMonth}
                                onChange={this.handleExpMonthChange}
                                error={!!this.state.expDateError}
                                inputProps={{
                                    'name': 'exp-month',
                                    'id': 'exp-month',
                                    'aria-describedby': 'exp-date-error'
                                }}
                            >
                                {
                                    this.state.expMonthOptions.map((month, i) => {
                                        return (
                                            <MenuItem key={i} value={month}>{month}</MenuItem>
                                        );
                                    })
                                }
                            </Select>
                        </FormControl>

                        <FormControl className={classes.expYearSelect}>
                            <InputLabel htmlFor="exp-year">Exp year</InputLabel>
                            <Select
                                value={this.state.expYear}
                                onChange={this.handleExpYearChange}
                                inputProps={{
                                    name: 'exp-year',
                                    id: 'exp-year',
                                }}
                            >
                                {
                                    this.state.expYearOptions.map((year, i) => {
                                        return (
                                            <MenuItem key={i} value={year}>{year}</MenuItem>
                                        );
                                    })
                                }
                            </Select>
                        </FormControl>
                        
                        {
                            this.state.expDateError ?
                            (
                                <FormHelperText id="exp-date-error" className={classes.errorText} error={true} aria-live="assertive">
                                    { this.state.expDateError }
                                </FormHelperText>
                            )
                            :
                            ''
                        }
                    </div>

                    <div className={classes.submitButton}>
                        <Button variant="contained" color="primary">
                            Submit
                        </Button>
                    </div>
                </form>
            </div>
        );
    }
}

export default withStyles(styles)(CreditCardForm);

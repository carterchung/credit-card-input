const numRegex = /^\d+$/;

let CardNumberErrors = {
    MISSING: 'Please enter a credit card number',
    NON_NUMERIC: 'Card number must consist of the numbers 0-9',
    INVALID_LENGTH: 'Please check your card number',
};

let CVVErrors = {
    MISSING: 'Please enter a CVV value',
    NON_NUMERIC: 'CVV must consist of the numbers 0-9',
    INVALID_LENGTH: 'Please check your CVV value',
};

function getCardType(cardNumber) {
    if(cardNumber[0] === '4') {
        return 'VISA';
    }

    if(cardNumber[0] === '3' && ['4','7'].includes(cardNumber[1]) ) {
        return 'AMEX';
    }

    return 'OTHER';
}

function isCardLengthValid(cardNumber) {
    const cardType = getCardType(cardNumber);
    
    if(cardType === 'VISA' && cardNumber.length !== 16) {
        return false;
    }

    if(cardType === 'AMEX' && cardNumber.length !== 15) {
        return false;
    }

    return true;
}

function getCardNumberErrorMessage(cardNumber) {
    if(cardNumber.length === 0) {
        return CardNumberErrors.MISSING;
    }

    if(!cardNumber.match(numRegex)) {
        return CardNumberErrors.NON_NUMERIC;
    }

    if(!isCardLengthValid(cardNumber)) {
        return CardNumberErrors.INVALID_LENGTH;
    }

    return '';
}

function getCVVErrorMessage(cvv, cardNumber) {
    if(cvv.length === 0) {
        return CVVErrors.MISSING;
    }

    if(!cvv.match(numRegex)) {
        return CVVErrors.NON_NUMERIC;
    }

    if(!isCVVLengthValid(cvv, cardNumber)) {
        return CVVErrors.INVALID_LENGTH;
    }

    return '';
}

function isCVVLengthValid(cvv, cardNumber) {
    const cardType = getCardType(cardNumber);

    if(cardType === 'VISA' && cvv.length !== 3) {
        return false;
    }

    if(cardType === 'AMEX' && cvv.length !== 4) {
        return false;
    }

    return true;
}

export {
    CardNumberErrors,
    CVVErrors,
    getCardType,
    isCardLengthValid,
    getCardNumberErrorMessage,
    getCVVErrorMessage,
    isCVVLengthValid,
};
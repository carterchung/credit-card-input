import {getExpMonthOptions, getExpYearOptions, getExpDateError} from './expirationDateUtils.js';

describe('getExpMonthOptions', function() {
    test('There should be 12 month options', () => {
        expect(getExpMonthOptions().length).toBe(12);
    });
});

describe('getExpYearOptions', function() {
    test('There should be 21 year options', () => {
        expect(getExpYearOptions(2018).length).toBe(21);
    });

    test('The first year option should be the current year', () => {
        expect(getExpYearOptions(2018)[0]).toBe(2018);
    });

    test('The last year option should be 20 years later', () => {
        const yearOptions = getExpYearOptions(2018);
        expect(yearOptions[yearOptions.length - 1]).toBe(2018 + 20);
    });
});

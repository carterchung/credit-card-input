function getExpMonthOptions() {
    return [
        '01',
        '02',
        '03',
        '04',
        '05',
        '06',
        '07',
        '08',
        '09',
        '10',
        '11',
        '12',
    ];
}

function getExpYearOptions(currentYear) {
    const yearsToShow = 21;
    const yearOptions = [];
    for (let i = 0; i < yearsToShow; i++) {
        yearOptions.push(currentYear + i);
    }
    return yearOptions;
}

function getExpDateError(currentYear, currentMonth, selectedMonth, selectedYear) {
    if(currentYear === selectedYear && selectedMonth < currentMonth) {
        return 'Expiration date cannot be in the past';
    }
    return '';
}

export {getExpMonthOptions, getExpYearOptions, getExpDateError};